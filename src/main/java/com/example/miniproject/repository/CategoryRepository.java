package com.example.miniproject.repository;

import com.example.miniproject.model.entity.Category;
import com.example.miniproject.model.entity.Task;
import com.example.miniproject.model.entity.UserInfo;
import com.example.miniproject.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Properties;

@Mapper
public interface CategoryRepository {

    @Select("""
            INSERT INTO category_tb (name,user_id) VALUES(#{request.categoryName},
             #{userId})
            RETURNING id
            """)
    Integer insertCategory(@Param("request") CategoryRequest categoryRequest,Integer userId);

    @Delete("""
            DELETE FROM category_tb where id = #{id} AND user_id = #{userId}
            """)
    Boolean deleteCategoryById(@Param("id") Integer categoryId,Integer userId);
    @Select("""
        SELECT * FROM category_tb LIMIT #{size} OFFSET #{size} * (#{page} -1 )
        """)
    List <Category> getAllCategory( Integer page, Integer size);

    @Select("""
        SELECT * FROM category_tb ORDER BY id ASC
                                  LIMIT #{size} OFFSET #{size} * (#{page} -1 )
        """)
    List <Category> getAllCategoryOrderAsc( Integer page, Integer size);

    @Select("""
                SELECT * FROM category_tb ORDER BY id DESC
                                  LIMIT #{size} OFFSET #{size} * (#{page} -1 )
            """)
    List <Category> getAllCategoryOrderDes( Integer page, Integer size);

    @Select("""
        SELECT * FROM category_tb WHERE id = #{id}
        """)
    Category getCategoryById(Integer categoryId);

    @Select("""
            SELECT * FROM category_tb WHERE id = #{CateId} AND user_id = #{currentUserId}
            """)
    Category getCategoryIdByCurrentUser(Integer currentUserId, Integer CateId);

    @Select("""
            UPDATE category_tb
                    SET name = #{request.categoryName}
                    WHERE id = #{categoryId} AND user_id = #{getUserCurrentId}
                    RETURNING id
                        
            """)
    Integer updateCategory(@Param("request") CategoryRequest categoryRequest, Integer categoryId, Integer getUserCurrentId);


    @Select("""
            SELECT * FROM category_tb WHERE user_id = #{id} ORDER BY id DESC
            LIMIT #{size} OFFSET #{size} * (#{page} -1)
            """)
    List<Category> getCategoryAllCurrentUser(@Param("id") Integer userId, Integer page, Integer size);


    @Select("""
        SELECT * FROM category_tb WHERE user_id = #{id} ORDER BY id ASC
                                  LIMIT #{size} OFFSET #{size} * (#{page} -1 )
        """)
    List <Category> getAllCurrentCategoryOrderAsc(@Param("id") Integer userId, Integer page, Integer size);

}
