package com.example.miniproject.exception;

public class UserEmailIsTakenException extends RuntimeException{
    public UserEmailIsTakenException() {
    }

    public UserEmailIsTakenException(String message) {
        super(message);
    }
}
