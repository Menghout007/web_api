package com.example.miniproject.exception;

public class EmptyFieldPasswordException extends RuntimeException{
    public EmptyFieldPasswordException() {
    }

    public EmptyFieldPasswordException(String message) {
        super(message);
    }
}
