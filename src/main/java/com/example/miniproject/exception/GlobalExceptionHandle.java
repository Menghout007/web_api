package com.example.miniproject.exception;

import org.bouncycastle.openssl.PasswordException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;
@ControllerAdvice
public class GlobalExceptionHandle {
    @ExceptionHandler(InvalidFieldCategoryException.class)
    ProblemDetail handleInvalidFieldCategoryException(InvalidFieldCategoryException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Invalid field !!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/categories"));
        return problemDetail;
    }

    @ExceptionHandler(EmptyFieldCategoryException.class)
    ProblemDetail handleEmptyFieldCategoryException(EmptyFieldCategoryException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Invalid field !!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/tasks/users"));
        return problemDetail;
    }

    @ExceptionHandler(EmptyFieldEmailException.class)
    ProblemDetail handleEmptyFieldEmailException(EmptyFieldEmailException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle(" field is empty !!");
        problemDetail.setStatus(404);
        problemDetail.setInstance(URI.create("/api/v1/auth"));
        return problemDetail;
    }

    @ExceptionHandler(InvalidFieldEmailExpcetion.class)
    ProblemDetail handleInvalidFieldEmailExpcetion(InvalidFieldEmailExpcetion e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/bad-request"));
        problemDetail.setTitle("Invalid Email field !!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/auth"));
        return problemDetail;
    }

    @ExceptionHandler(UserEmailIsNotFoundException.class)
    ProblemDetail UserEmailIsNotFoundException(UserEmailIsNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/bad-request"));
        problemDetail.setTitle("Email is not found !!");
        problemDetail.setStatus(404);
        problemDetail.setInstance(URI.create("/api/v1/auth"));
        return problemDetail;
    }

    @ExceptionHandler(InvalidLoginPasswordException.class)
    ProblemDetail InvalidLoginPasswordException(InvalidLoginPasswordException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/bad-request"));
        problemDetail.setTitle("Password is incorrect !!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/auth"));
        return problemDetail;
    }

    @ExceptionHandler(UserEmailIsTakenException.class)
    ProblemDetail handleUserEmailIsTakenException(UserEmailIsTakenException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/bad-request"));
        problemDetail.setTitle("Email is already taken !!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/auth"));
        return problemDetail;
    }

    @ExceptionHandler(EmptyFieldPasswordException.class)
    ProblemDetail handleEmptyFieldPasswordException(EmptyFieldPasswordException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/bad-request"));
        problemDetail.setTitle("Password Field is empty!!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/auth"));
        return problemDetail;
    }

    @ExceptionHandler(InvalidPasswordException.class)
    ProblemDetail handleInvalidPasswordException(InvalidPasswordException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/bad-request"));
        problemDetail.setTitle("Password Invalid !!");
        problemDetail.setDetail("Password must contain at least one lowercase " +
                " one uppercase " +
                "one special character" +
                " one digit" +
                " length of at least 8 characters and a maximum of 20 characters." +
                " !!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/auth"));
        return problemDetail;
    }


    @ExceptionHandler(InvalidFieldTaskException.class)
    ProblemDetail handleEmptyFieldTaskException(InvalidFieldTaskException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Invalid field !!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/tasks/users"));
        return problemDetail;
    }

    @ExceptionHandler(EmptyFieldTaskException.class)
    ProblemDetail handleEmptyFieldTaskException(EmptyFieldTaskException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Empty field !!");
        problemDetail.setStatus(400);
        problemDetail.setInstance(URI.create("/api/v1/tasks/users"));
        return problemDetail;
    }

}
