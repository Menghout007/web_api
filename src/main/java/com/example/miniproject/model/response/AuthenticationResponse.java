
package com.example.miniproject.model.response;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthenticationResponse<T> {

    private Integer userId;
    private String email;

    private String token;
}
