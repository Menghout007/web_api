package com.example.miniproject.model.response;

import lombok.*;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponse <T>{
    private T payload;
    private Timestamp date;
    private boolean success;
}
