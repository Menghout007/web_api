package com.example.miniproject.service;


import com.example.miniproject.exception.InvalidLoginPasswordException;
import com.example.miniproject.model.request.AuthenicationRequest;
import com.example.miniproject.model.response.AuthenticationResponse;
import com.example.miniproject.repository.UserRepository;
import com.example.miniproject.securityConfig.JwtAuthnicationFliter;
import com.example.miniproject.securityConfig.JwtUnit;
import com.example.miniproject.service.serviceImp.UserServiceImp;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final JwtAuthnicationFliter jwtAuthenticationFilter;
    private final JwtUnit jwtUtil;
    private final UserServiceImp userServiceImp;
    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    public AuthenticationResponse authenticate(AuthenicationRequest request) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getEmail(),
                            request.getPassword()
                    ));
        } catch (Exception e)
        {
            throw new InvalidLoginPasswordException();
        }

        var user = userRepository.findByEmail(request.getEmail());

                var jwtToken = jwtUtil.generateToken(user);
                return AuthenticationResponse.builder()
                        .userId(user.getId())
                        .email(user.getEmail())
                        .token(jwtToken).build();

    }

}
