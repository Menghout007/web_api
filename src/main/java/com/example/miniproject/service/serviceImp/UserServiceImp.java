package com.example.miniproject.service.serviceImp;

import com.example.miniproject.model.request.AuthenicationRequest;
import com.example.miniproject.model.entity.UserInfo;
import com.example.miniproject.repository.UserRepository;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo userInfo = userRepository.findByEmail(username);
        if (userInfo == null) {
            throw new UsernameNotFoundException("user not found");
        }
        System.out.println(userInfo);
        return userInfo;
    }

    public Integer addNewUser(AuthenicationRequest authenicationRequest) {
        return userRepository.addNewUser(authenicationRequest);
    }

    public Integer getUserByEmail(String userEmail){
        return userRepository.getUserByEmail(userEmail);
    }

}
