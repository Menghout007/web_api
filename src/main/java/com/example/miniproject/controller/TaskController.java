package com.example.miniproject.controller;

import com.example.miniproject.exception.InvalidFieldCategoryException;
import com.example.miniproject.exception.InvalidFieldTaskException;
import com.example.miniproject.model.entity.Task;
import com.example.miniproject.model.entity.UserInfo;
import com.example.miniproject.model.request.TaskRequest;
import com.example.miniproject.model.response.TaskResponse;
import com.example.miniproject.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@SecurityRequirement(name = "bearerAuth")
public class TaskController {
    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }
    public Integer getUserCurrentId(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = (UserInfo) authentication.getPrincipal();
        int userId = userInfo.getId();
        return userId;
    }

    @GetMapping("/tasks/users")
    @Operation(summary = "Get all task for current user")
    public ResponseEntity<TaskResponse<List<Task>>> getAllTaskCurrentUser(
            @RequestParam boolean asc, @RequestParam boolean desc,
            @RequestParam Integer page, @RequestParam Integer size
    ){
        if(asc && !desc) {
            TaskResponse<List<Task>> response = TaskResponse.<List<Task>>builder()
                    .payload(taskService.getAllTaskCurrentUser(getUserCurrentId(),page,size))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else if (desc && !asc) {
            TaskResponse<List<Task>> response = TaskResponse.<List<Task>>builder()
                    .payload(taskService.getAllTaskCurrentUserOrderByDesc(getUserCurrentId(),page,size))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else if (asc && desc)
        {
            throw new InvalidFieldTaskException("Invalid !");
        }else {
            throw new InvalidFieldTaskException("Invalid !");
        }

    }

    @GetMapping("/tasks")
    @Operation(summary = "get All Task")
    public List<Task> getAllTask(@RequestParam boolean asc, @RequestParam boolean desc,
            @RequestParam Integer page, @RequestParam Integer size
    ){
        if(asc && !desc) {
            return taskService.getAllTask(page, size);
        }
        else if (desc && !asc) {
            return taskService.getAllTaskByOrderDes(page,size);
        }
        else if (asc && desc)
        {
            throw new InvalidFieldTaskException("Invalid !");
        }else {
            throw new InvalidFieldTaskException("Invalid !");
        }
    }

    @GetMapping("/tasks/{id}")
    @Operation(summary = "Get task by id")
    public ResponseEntity<TaskResponse<Task>> getTaskById(@PathVariable("id") Integer taskId) {
        TaskResponse<Task> response = TaskResponse.<Task>builder()
                .payload(taskService.getTaskById(taskId))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/tasks/{id}/users")
    @Operation(summary = "Get task by id for current user")
    public ResponseEntity<TaskResponse<Task>> getTaskByIdCurrentUser(@PathVariable("id") Integer taskId){
        TaskResponse<Task> response = TaskResponse.<Task>builder()
                .payload(taskService.getAllTaskByIdCurrentUser(taskId,getUserCurrentId()))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }


    @PostMapping("/tasks/users")
    @Operation(summary = "Add new task")
    public ResponseEntity<TaskResponse<Task>> addNewTask(@RequestBody TaskRequest taskRequest){
        Integer storeNewTaskId = taskService.addNewTask(taskRequest,getUserCurrentId());
        System.out.println("task new id: " +storeNewTaskId);
        TaskResponse<Task> response = TaskResponse.<Task>builder()
                .payload(taskService.getAllTaskByIdCurrentUser(taskRequest.getCategoryId(),getUserCurrentId()))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/tasks/{id}/users")
    @Operation(summary = "Delete task by id for current user")
    public ResponseEntity<TaskResponse<String>> deleteTaskById(@PathVariable("id") Integer taskId){
        TaskResponse<String> response = null;
        if (taskService.deleteTask(taskId,getUserCurrentId()) == true){
            response = TaskResponse.<String>builder()
                    .payload("Delete this task id "+ taskId + " is successful !!")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
        }
        return ResponseEntity.ok(response);
    }


    @GetMapping("/tasks/status/users")
    @Operation(summary = "filter task by status for current user")
    public ResponseEntity<TaskResponse<List<Task>>> getTaskByStatusCurrentUser(@RequestParam("status") String status){
        if(taskService.getTaskByStatusCurrentUser(getUserCurrentId(),status).isEmpty())
        {
            throw new InvalidFieldTaskException("There are no " + status + " in list !!");
        }else {
            TaskResponse<List<Task>> response = TaskResponse.<List<Task>>builder()
                    .payload(taskService.getTaskByStatusCurrentUser(getUserCurrentId(),status))
                    .date(new Timestamp(System.currentTimeMillis()))
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @PutMapping("/tasks/{id}")
    @Operation(summary = "Update Tasks by Id")
    public ResponseEntity<TaskResponse<Task>> updateTask(@PathVariable("id")  Integer taskId, @RequestBody TaskRequest taskRequest) {
        Integer storeId = taskService.updateTaskById(taskId,taskRequest);
        TaskResponse<Task> response = TaskResponse.<Task>builder()
                .payload(taskService.getTaskById(storeId))
                .date(new Timestamp(System.currentTimeMillis()))
                .success(true)
                .build();
        return ResponseEntity.ok().body(response);
    }
}
