CREATE TABLE user_tb (
    id SERIAL PRIMARY KEY ,
    email VARCHAR(255) NOT NULL UNIQUE ,
    password VARCHAR(255) NOT NULL ,
    role varchar(255) DEFAULT 'ROLE_USER'
);


CREATE TABLE category_tb(
    id SERIAL PRIMARY KEY ,
    name VARCHAR(255) NOT NULL ,
    date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    user_id INT REFERENCES user_tb(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE task_tb(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL ,
    description VARCHAR(255) NOT NULL ,
    date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    status VARCHAR(255) NOT NULL ,
    user_id INT REFERENCES user_tb(id) ON DELETE CASCADE ON UPDATE CASCADE ,
    category_id INT REFERENCES category_tb(id) ON DELETE CASCADE ON UPDATE CASCADE
);


INSERT INTO user_tb (email, password) VALUES ('Menghout@gmail.com','$2a$04$D5I.wCx81JZByUCxoun9Tus/FtetvMN2j2OV2KyNZfTZS27VpR8zW'),
                                             ('Nary@gmail.com','$2a$04$D5I.wCx81JZByUCxoun9Tus/FtetvMN2j2OV2KyNZfTZS27VpR8zW'),
                                             ('Satyarith@gmail.com','$2a$04$D5I.wCx81JZByUCxoun9Tus/FtetvMN2j2OV2KyNZfTZS27VpR8zW'),
                                             ('user','$2a$04$D5I.wCx81JZByUCxoun9Tus/FtetvMN2j2OV2KyNZfTZS27VpR8zW');

INSERT INTO category_tb (name, user_id) VALUES ('My Task',1),
                                               ('My Task',2),
                                               ('School Task',2);

INSERT INTO task_tb (name, description, status, user_id, category_id) VALUES
                                                                          ('Reading Book','Read Korean Book','in_progress',2,1),
                                                                          ('Reading Book','Read Korean Book','in_progress',3,19),
                                                                          ('Reading Book','Read Korean Book','in_progress',4,1);



SELECT * FROM task_tb where user_id = 6 order by id
                  LIMIT 2 OFFSET 0

